import { Module } from '@nestjs/common';
import { ThirdController } from './new.controller';
import { ThirdService } from './new.service';
import {AnotherModule} from './another.module';

@Module({
  imports: [AnotherModule],
  controllers: [ThirdController],
  providers: [ThirdService],
  exports:[AnotherModule]  //it'll be available for other modules as well 
 
})
export class ThirdModule {}