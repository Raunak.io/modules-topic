import { Injectable } from '@nestjs/common';

@Injectable()
export class NewService {
  getMessage(): string {
    return 'Hello ......!';
  }
}

@Injectable()
export class ThirdService {
  getMessageAgain(): string {
    return 'Hello ......!';
  }
}
