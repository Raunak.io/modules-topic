import { Module,Global } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {AnotherModule} from './another.module';
import {ThirdModule} from './third.module';
// @Global() to make any module global scoped  declared only once
@Module({
  imports: [AnotherModule,ThirdModule],
  controllers: [AppController],
  providers: [AppService],

})
export class AppModule {}
