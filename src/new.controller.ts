import { Controller, Get } from '@nestjs/common';
import { NewService,ThirdService } from './new.service';

@Controller('test')
export class NewController {
  constructor(private readonly newService: NewService) {}

  @Get()
  getDat(): string {
    return this.newService.getMessage();
  }
}

@Controller('third')
export class ThirdController {
  constructor(private readonly newService: NewService,private readonly thirdService:ThirdService) {}

  @Get()
  getDatAgain(): string {
    const data = this.thirdService.getMessageAgain();
    console.log(data);
    return this.newService.getMessage();
  }
}
